<?php
echo "<br>Задание 1: 
  Допустим, вы разрабатываете систему управления данными компании, которая имеет несколько отделов: отдел продаж, отдел разработки, отдел маркетинга и т.д. Каждый отдел имеет своих сотрудников, которые имеют различные роли и доступы к разным частям системы.

Для этого вы можете создать следующие классы:

Класс Employee (Сотрудник) с следующими свойствами:

name - имя сотрудника
email - адрес электронной почты сотрудника
password - пароль для входа в систему
department - отдел, в котором работает сотрудник
role - роль сотрудника (администратор, менеджер, обычный пользователь и т.д.)

Класс Employee должен иметь следующие методы:

__construct(name, email, password, department, role) - конструктор класса, который принимает все свойства сотрудника в качестве аргументов и устанавливает их соответствующим образом

getName() - возвращает имя сотрудника
getEmail() - возвращает адрес электронной почты сотрудника
getPassword() - возвращает пароль сотрудника
getDepartment() - возвращает отдел, в котором работает сотрудник
getRole() - возвращает роль сотрудника


Класс Department (Отдел) с следующими свойствами:
name - название отдела

Класс Department должен иметь следующие методы:

__construct(name) - конструктор класса, который принимает идентификатор и название отдела в качестве аргументов и устанавливает их соответствующим образом
getName() - возвращает название отдела



Класс Permission (Разрешение) с следующими свойствами:
name - название разрешения


Класс Permission должен иметь следующие методы:

__construct(id, name) - конструктор класса, который принимает идентификатор и название разрешения в качестве аргументов и устанавливает их соответствующим образом

getId() - возвращает идентификатор разрешения
getName() - возвращает название разрешения


Класс Role (Роль) с следующими свойствами:
name - название роли
permissions - массив объектов класса Permission, которые имеются у данной роли


Класс Role должен иметь следующие методы:

__construct(name) - конструктор класса, который принимает идентификатор и название роли в качестве аргументов и устанавливает их соответствующим образом
getName() - возвращает название роли
getPermissions() - возвращает массив объектов класса Permission, которые имеются у данной роли
addPermission(permission) - добавляет объект класса Permission в массив permissions
removePermission(permission) - удаляет объект класса Permission из массива permissions


Класс AccessControl (Управление доступом) с следующими свойствами:

roles - массив объектов класса Role, которые имеются в системе
permissions - массив объектов класса Permission, которые имеются в системе


Класс AccessControl должен иметь следующие методы:

__construct(roles, permissions) - конструктор класса, который принимает массив объектов класса Role и массив объектов класса Permission в качестве аргументов и устанавливает их соответствующим образом checkAccess(employee, permission) - проверяет, имеет ли сотрудник employee доступ к разрешению permission. Метод должен вернуть true, если сотрудник имеет доступ, и false в противном случае. <br>";



class Department {
        public function __construct (
            private int $idDepartment,
            private string $nameDepartment,) {
                $this->idDepartment = $idDepartment;
                $this->nameDepartment = $nameDepartment;
        }
        public function getName($nameDepartment): string
        {
            return $this->nameDepartment;
        }
}


class Permission  {
    public function __construct (
        private int $id,
        private string $namePermission,) {
        $this->id = $id;
        $this->namePermission = $namePermission;
    }
    public function getName($namePermission): string
    {
        return $this->namePermission;
    }
    public function getId($id):int
    {
        return $this->idPermission;
    }
}


class Role  {
    public function __construct (
        private string $nameRole,
        private array $permissions=[],) {
        $this->nameRole = $nameRole;
        $this->permissions = $permissions;
    }
    public function getName($nameRole): string
    {
        return $this->nameRole;
    }
    public function getPermissions($permissions):array
    {
        return $this->permissions;
    }
    public function addPermission (Permission $permission){
        $this->permissions=$permission;
    }
    public function removePermission (Permission $permission){
            unset ($permission);
    }
}



class Employee  {
    public function __construct (
        private string $nameEmployee,
        private email $email,
        private Department $department,
        private Role $role,) {
        $this->nameEmployee = $nameEmployee;
        $this->email = $email;
        $this->department = $department;
        $this->role = $role;
    }
    public function getName($nameEmployee): string
    {
        $this->nameEmployee;
    }
    public function getEmail($email):email
    {
        return $this->email;
    }
    public function getDepartment($department):Department
    {
        return $this->department;
    }
    public function getRole($role):Role
    {
        return $this->role;
    }
}


class AccessControl  {
    public function __construct (
        private array $roles,
        private array $permissions,) {
        $this->roles = $roles;
        $this->permissions = $permissions;
    }
    public function checkAccess( Employee $employee, Permission $permission): bool
    {
        foreach($employee->getRole()->getPermissions() as $employeeRolePermission){
            if($employeeRolePermission->getId() == $permission->getId()){
                return true;
            }
        }
        return false;
    }
}

$managers = new Department (35, 'Managers');
$accounting = new Department (02, 'Accounting');

$permission1 = new Permission (238, 'Give permission');
$permission2 = new Permission (241, 'Remove permission');

$manager = new Role ('Manager', [$permission1]);
$accountant = new Role ('Accountant', [$permission2]);
$manager ->addPermission($permission1);
$accountant ->addPermission($permission2);
$manager -> removePermission ($permission1);

$fedor = new Employee ('Fedor', 'fedor1@mail.com', $managers, $manager);
$anna = new Employee ('Anna','anna2@mail.com', $accounting, $accountant);

$control1 = new AccessControl ('Fedor', 'fedor1@mail.com', $manager, $permission1);
$control1 = new AccessControl ('Anna','anna2@mail.com', $accountant, $permission2);
$control1 ->checkAccess($fedor,$permission1 );
$control2 ->checkAccess($fedor,$permission2 );
