<?php
echo "<br>Задание 2: 
   Допустим, у нас есть несколько классов: Vehicle, Car и Motorcycle.

— Vehicle - базовый класс, который определяет общие свойства и методы для всех транспортных средств.
— Car - класс автомобилей, наследуется от Vehicle и имеет свойства и методы, специфичные для автомобилей (например, количество дверей, максимальная скорость).
— Motorcycle - класс мотоциклов, наследуется от Vehicle и имеет свойства и методы, специфичные для мотоциклов (например, тип мотоцикла, максимальная скорость).

Задача заключается в том, чтобы создать систему, которая будет моделировать использование различных видов транспорта. Например, мы можем создать объекты Car и Motorcycle и использовать методы start() и stop() для моделирования запуска и остановки двигателя.

Также можно добавить дополнительные классы, такие как Truck, Bus и Bicycle, которые будут определять различные виды транспорта и их свойства, и будут использоваться для различных целей в системе. Например, класс Bicycle может иметь методы для моделирования торможения и тд. <br>";
class Vehicle {

        private string $name,
        private string $model,
        private string $color,
        private int $year;

        public function getName($name): string 
        {
            return $this->name;
        }
        public function getModel($model): string
        {
            return $this->model;
        }       
        public function getColor($color): string
        {
            return $this->color;
        }       
        public function getYear($year): int
        {
            return $this->year;
        }
        public function getFeatures(){
         return "марка " . $this->name .", модель " . $this->model . ", цвет " .$this->color . ", год выпуска " . $this->year . ".";
        }
}

trait Engine {
    public function startEngine()
    {
        if ($this->started = false) {
            echo "Вы завели двигатель. ";
        } else {
            echo "Мотор заглушен. ";
        }
    }
}
class Car extends Vehicle {
        use Engine;
       private int $maxspeed;
       private int $doors;
       private bool $started;
       public function __construct ($name, $model, $color, $year, $maxspeed, $doors, $started)
        {
           parent::__construct ($name, $model, $color, $year);
           $this->maxspeed = $maxspeed;
           $this->doors = $doors;
           $this->started = $started;
        }
        public function getCar(){
        return ", максимальная скорость " . $this->maxspeed ."км/ч, количество дверей ". $this->doors . ". ";
        }

        public function startEngine()
        {
            if ($this->started = false) {
                echo "Вы завели двигатель.";
            } else {
                echo "Мотор заглушен.";
            }
        }
}

class Motorcycle extends Vehicle {
    use Engine;
    private int $maxspeed;
    public string $type;
    private bool $started;
    public function __construct ($name, $model, $color, $year, $maxspeed, $type, $started)
    {
        parent::__construct ($name, $model, $color, $year);
        $this->maxspeed = $maxspeed;
        $this->type = $type;
        $this->started = $started;
    }
    public function getMotorcycle(){
        return "тип мотоцикла - ". $this->type . ", максимальная скорость " . $this->maxspeed ."км/ч. ";
    }
    public function startEngine()
    {
        if ($this->started = false) {
            echo "Вы завели двигатель. ";
        } else {
            echo "Мотор заглушен. ";
        }
    }
}

    $car = new Car (name:'BMW', color:'green', year:2015, model:'X6', doors:2, maxspeed:220, started:1 );
    echo '<br>';
    echo "Информация об автомобиле: ";
    echo $car -> getFeatures();
    echo $car -> getCar();
    echo $car -> startEngine();
    echo '<br>';


    $motorcycle = new Motorcycle (name:'Yamaha', color:'black', year:2006, model:'YZF-R1SP', maxspeed:250, type:'sport', started:true);
    echo '<br>';
    echo "Информация о мотоцикле: ";
    echo $motorcycle -> getFeatures();
    echo $motorcycle -> getMotorcycle();
    echo $motorcycle -> startEngine();





